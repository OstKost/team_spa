import React from 'react'
import Layout from './Hoc/Layout'
import { Switch } from 'react-router-dom'

import PrivateRoute from './Components/authRoutes/privateRoutes'
import PublicRoute from './Components/authRoutes/publicRoutes'

import Home from './Components/home'
import SignIn from './Components/signin'
import Dashboard from './Components/admin/Dashboard'
import AdminMatches from './Components/admin/matches';
import AddEditMatch from './Components/admin/matches/addEditMatch';
import AdminPlayers from './Components/admin/players';
import AddEditPlayer from './Components/admin/players/addEditPlayer';

const Routes = props => {
	return (
		<Layout>
			<Switch>
				<PrivateRoute
					{...props}
					restricted={false}
					path="/admin_players/edit_player"
					exact
					component={AddEditPlayer}
				/>
				<PrivateRoute
					{...props}
					restricted={false}
					path="/admin_players/edit_player/:id"
					exact
					component={AddEditPlayer}
				/>
				<PrivateRoute
					{...props}
					restricted={false}
					path="/admin_players"
					exact
					component={AdminPlayers}
				/>
				<PrivateRoute
					{...props}
					restricted={false}
					path="/admin_matches/edit_match"
					exact
					component={AddEditMatch}
				/>
				<PrivateRoute
					{...props}
					restricted={false}
					path="/admin_matches/edit_match/:id"
					exact
					component={AddEditMatch}
				/>
				<PrivateRoute
					{...props}
					restricted={false}
					path="/admin_matches"
					exact
					component={AdminMatches}
				/>
				<PrivateRoute
					{...props}
					restricted={false}
					path="/dashboard"
					exact
					component={Dashboard}
				/>
				<PublicRoute
					{...props}
					restricted={true}
					path="/signIn"
					exact
					component={SignIn}
				/>
				<PublicRoute {...props} path="/" exact component={Home} />
			</Switch>
		</Layout>
	)
}

export default Routes
