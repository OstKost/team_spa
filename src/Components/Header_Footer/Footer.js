import React from 'react'
import { CityLogo } from '../UI/icons'

const Footer = () => {
	return (
		<footer className="bck_blue">
			<div className="footer+logo">
				<CityLogo link={true} linkTo="/" width="70px" heigth="70px" />
			</div>
			<div className="footer_discl">
				Manchester City {new Date().getFullYear()}. All rights reserved.
			</div>
		</footer>
	)
}

export default Footer
