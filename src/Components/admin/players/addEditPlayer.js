import React, { Component } from 'react'
import AdminLayout from '../../../Hoc/AdminLayout'

import FormField from '../../UI/formFields'
import { validate } from '../../UI/misc'

import { firebasePlayers, firebaseDB, firebase } from '../../../firebase'
import Fileuploader from '../../UI/fileUploader'

export default class AddEditPlayer extends Component {
	state = {
		playerId: '',
		formType: '',
		formError: false,
		formSuccess: '',
		defaultImg: '',
		formData: {
			name: {
				element: 'input',
				value: '',
				config: {
					label: 'Player Name',
					name: 'name_input',
					type: 'text'
				},
				validation: {
					required: true
				},
				valid: false,
				validationMessage: '',
				showLabel: true
			},
			lastname: {
				element: 'input',
				value: '',
				config: {
					label: 'Player Last name',
					name: 'lastname_input',
					type: 'text'
				},
				validation: {
					required: true
				},
				valid: false,
				validationMessage: '',
				showLabel: true
			},
			number: {
				element: 'input',
				value: '',
				config: {
					label: 'Player Number',
					name: 'number_input',
					type: 'number'
				},
				validation: {
					required: true
				},
				valid: false,
				validationMessage: '',
				showLabel: true
			},
			position: {
				element: 'select',
				value: '',
				config: {
					label: 'Select a position',
					name: 'select_position',
					type: 'select',
					options: [
						{ key: 'Keeper', value: 'Keeper' },
						{ key: 'Defence', value: 'Defence' },
						{ key: 'Midfield', value: 'Midfield' },
						{ key: 'Striker', value: 'Striker' }
					]
				},
				validation: {
					required: true
				},
				valid: false,
				validationMessage: '',
				showLabel: true
			},
			image: {
				element: 'image',
				value: '',
				validation: {
					required: true
				},
				valid: false,
				validationMessage: '',
				showLabel: true
			}
		}
	}

	updateForm(element, content = null) {
		const newFormData = { ...this.state.formData }
		const newElement = { ...newFormData[element.id] }

		if (content) {
			newElement.value = content
		} else {
			newElement.value = element.event.target.value
		}

		let valiData = validate(newElement)
		newElement.valid = valiData[0]
		newElement.validationMessage = valiData[1]

		newFormData[element.id] = newElement

		this.setState({
			formData: newFormData,
			formError: false
		})
	}

	successForm = message => {
		this.setState({
			formSuccess: message
		})
		setTimeout(() => {
			this.setState({
				formSuccess: ''
			})
		}, 2000)
	}

	async submitForm(event) {
		event.preventDefault()

		let dataToSubmit = {}
		let formIsValid = true

		for (let key in this.state.formData) {
			dataToSubmit[key] = this.state.formData[key].value
			formIsValid = this.state.formData[key].valid && formIsValid
		}

		if (formIsValid) {
			if (this.state.formType === 'Edit player') {
				try {
					await firebaseDB
						.ref(`players/${this.state.playerId}`)
						.update(dataToSubmit)
					this.successForm('Update correctly')
				} catch (error) {
					this.setState({
						formError: true
					})
				}
			} else {
				try {
					const player = await firebasePlayers.push(dataToSubmit)
					console.log(player)
					this.props.history.push('/admin_players')
				} catch (error) {
					console.log(error)
					this.setState({
						formError: true
					})
				}
			}
		} else {
			this.setState({
				formError: true
			})
		}
	}

	resetImg = () => {
		const newFormData = { ...this.state.formData }
		newFormData['image'].value = ''
		newFormData['image'].valid = false
		this.setState({
			defaultImg: '',
			formData: newFormData
		})
	}

	storeFilename = filename => {
		this.updateForm({ id: 'image' }, filename)
	}

	updateField = (player, playerId, formType, defaultImg) => {
		const newFormData = { ...this.state.formData }

		for (let key in newFormData) {
			newFormData[key].value = player[key]
			newFormData[key].valid = true
		}

		this.setState({
			formData: newFormData,
			playerId,
			defaultImg,
			formType
		})
	}

	async componentDidMount() {
		const playerId = this.props.match.params.id

		if (!playerId) {
			this.setState({
				formType: 'Add player'
			})
		} else {
			const playerData = await firebaseDB
				.ref(`players/${playerId}`)
				.once('value')
				.then(snapshot => snapshot.val())

			try {
				const imageURL = await firebase
					.storage()
					.ref('players')
					.child(playerData.image)
					.getDownloadURL()

				this.updateField(playerData, playerId, 'Edit player', imageURL)
			} catch (error) {
				this.updateField(
					{ ...playerData, image: '' },
					playerId,
					'Edit player',
					''
				)
				this.successForm(error.message)
			}
		}
	}

	render() {
		return (
			<AdminLayout>
				<div className="editplayers_dialog_wrapper">
					<h2>{this.state.formType}</h2>
					<div>
						<form onSubmit={event => this.submitForm(event)}>
							<Fileuploader
								dir="players"
								tag="Player image"
								defaultImg={this.state.defaultImg}
								defaultImgName={this.state.formData.image.value}
								resetImg={() => this.resetImg()}
								filename={filename =>
									this.storeFilename(filename)
								}
							/>
							<FormField
								id={'name'}
								formData={this.state.formData.name}
								change={element => this.updateForm(element)}
							/>
							<FormField
								id={'lastname'}
								formData={this.state.formData.lastname}
								change={element => this.updateForm(element)}
							/>
							<FormField
								id={'number'}
								formData={this.state.formData.number}
								change={element => this.updateForm(element)}
							/>
							<FormField
								id={'position'}
								formData={this.state.formData.position}
								change={element => this.updateForm(element)}
							/>

							<div className="success_label">
								{this.state.formSuccess}
							</div>
							{this.state.formError ? (
								<div className="error_label">
									Something is wrong
								</div>
							) : (
								''
							)}
							<div className="admin_submit">
								<button
									onClick={event => this.submitForm(event)}
								>
									{this.state.formType}
								</button>
							</div>
						</form>
					</div>
				</div>
			</AdminLayout>
		)
	}
}
