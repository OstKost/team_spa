import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'
import 'firebase/storage'

const config = {
	apiKey: 'AIzaSyDbMq8-PLCgJJl3P8nMF5mqaRJe3PPoGoQ',
	authDomain: 'team-spa-test.firebaseapp.com',
	databaseURL: 'https://team-spa-test.firebaseio.com',
	projectId: 'team-spa-test',
	storageBucket: 'team-spa-test.appspot.com',
	messagingSenderId: '362237140477'
}

firebase.initializeApp(config)

const firebaseDB = firebase.database()
const firebaseMatches = firebaseDB.ref('matches')
const firebasePromotions = firebaseDB.ref('promotions')
const firebaseTeams = firebaseDB.ref('teams')
const firebasePlayers = firebaseDB.ref('players')

export { firebase, firebaseDB, firebaseMatches, firebasePromotions, firebaseTeams, firebasePlayers }
